<?
/*
	Routing класс
	Требуется для разбора ссылки и нахождения соотношения по записям routing
	Разработана возможность самому настроить легко ссылки внутри web приложения без измениня mod_rewrite
	Так же есть возможность создать ограничения только методам GET или POST. Например к стрнанице /user/delete/1 можно переходить только через POST запрос.
	Встроена без зименения названия контроллера или метода контроллера изменять ссылки. Например к контроллеру users методу show с id = 10 можно обращаться по "родной ссылке" /users/show/id/10 или через настроку routing /user/10
	Список своих роутингов лежат в файле /route.php
*/
class Routing
{

	public static function UriParce()
	{
		//подгружаем свои кастомные роутинги
		include(SITE_PATH.'/route.php');

		//флаг того что кастомный роутинг найден
		$RoteFound = false;
		//переделанная ссылка, чтобы обращаться к контроллеру
		$ChangedUri = false;

		//перебираем роутинг
		foreach ($GlobalRoute as $route_preg => $end_point_object)
		{
			//если указан метод и он не соответствует текущему запросу, то продолжаем перебор
			//если сейчас обращаемся через POST, а в текущем роутинги указан только GET, про продолжаем перебор
			if(isset($end_point_object['method']) && $end_point_object['method'] != $_SERVER['REQUEST_METHOD'])
			{
				continue;
			}
			//универсальный роутинг, соответствует тому, что можно обращаться напрямую к контроллеру и методу. /users/show/id/10 Контроллер usersController метод show() аргументы array( id => 10)
			if($route_preg=='*')
			{
				//роутинг найден, изменять ссылку не надо, прекращаем перебор
				$RoteFound = true;
				$ChangedUri = $_SERVER['REQUEST_URI'];
				break;
			}
			//если соответствует нашему регулярному выразению, то запускаем механизм адаптации
			if(preg_match("/^{$route_preg}$/", $_SERVER['REQUEST_URI']))
			{
				//Меняем начальную ссылку на конечную. Изначально мы не знаем к какому контроллеру обращаться, в роутинге прописано значение контроллера и метода
				//Если изначально было /user/1 то в роутинге прописано, что именяем на /users/show/id/10 что соответствует корректному значению опеределения путей и подгрузки файлов
				$ChangedUri = preg_replace("/^{$route_preg}$/", $end_point_object['end'], $_SERVER['REQUEST_URI']);
				//Если указано, что контент статический, т.е. не требуется обращение к контроллерам, то возвращаем ответ с путем до файла а если указано, то какому контету соответствует, например css или javascript
				if($end_point_object['content'] == CONTENT_STATIC)
				{
					//controller - имя реального контроллера из файла /app/controllers
					//function - имя метода в контроллере
					//params - параметры, полученные от пользователя
					//static_file - путь до статического файла, который требуется передать пользователю, например js, css или html файл
					$Point = array('controller' => false, 'function' => false, 'params' => false, 'static_file' => $ChangedUri);
					if(isset($end_point_object['content_type']))
					{
						$Point['content_type'] = $end_point_object['content_type'];
					}
					return (object)$Point;
				}
				//Иначе указываем, что роутинг найден, обрываем перебор
				else
				{
					$RoteFound = true;
					break;
				}
			}
		}
		//Если при переборе роутинг не найден, то возвращаем ответ с указанием того, что пользователю надо вывести страницу с кодом 404 ошибки
		if($RoteFound==false)
		{
			//code - код ответа http пользователю
			//code_memo - подпись к коду
			return (object)array('controller' => false, 'function' => false, 'params' => false, 'code' => '404', 'code_memo' => 'Not found');
		}
		//разбиваем ссылку на части
		// /users/show/id/10
		// Разбивается на usersController
		// Метод контроллера show()
		// Параметры id => 10
		$uriSplit = explode('/', $ChangedUri,4);
		//получение имени контроллера, передаем разбитый массив, индекс в массиве и что возвращать, если имя контроллера отсутствует, в данном случае это root контроллер
		$controller = self::ParcePartials($uriSplit,1,'root');
		//получение имени метода контроллера, передаем разбитый массив, индекс в массиве и что возвращать, если имя метода отсутствует, в данном случае это index метод
		$function = self::ParcePartials($uriSplit, 2, 'index');
		//передаем параметры, для конвертации из формата /page/1/id/12/found
		// в формат array( page => 1, id => 12, found => false)
		$params = Routing::ParceParams(self::ParcePartials($uriSplit, 3));
		//Возвращаем обект с указанием имени контроллера, метода и параметров
		return (object)array('controller' => $controller, 'function' => $function, 'params' => $params);
	}
	/*
	 $uriArray - массив разбитой ссылки из метода UriParce вида /users/show/id/10
	 $index - индекс в массиве, для обращения к имени контроллеру или меода
	 $default - что возвращать, если мя не найдено
	*/
	private static function ParcePartials($uriArray, $index=1, $default=false)
	{
		//обращаемся к массиву по индексу
		return (!isset($uriArray[$index]) || strlen($uriArray[$index])==0) ? $default : $uriArray[$index];
	}

	/*
		Парсин параметров
		$ParamString - строка вида id/2/page/19/flag ,для конвертации в массив вида ключ-значение
		array( id => 2, page => 18, flag => false)
	*/
	private static function ParceParams($ParamString)
	{
		//Разбиваем ссылку с разделителем /
		$params = explode('/', $ParamString);
		//Создаем пустой пассив для возврата
		$args = array();
		//Флаг для выбора записи между ключем и значением
		$parceFlag = false;
		//Перебираем параметры
		foreach ($params as $paramValue)
		{
			//Если значение переменной flag == false
			//то создаем в массив args[] значение с ключем, имя которого текущее значение $paramValue из массива перебора и указываем значение флага тем же именем
			//Иначе находим значение в массиве args по ключу $parceFlag и передаем значение $paramValue

			if($parceFlag===false)
			{
				$args[$paramValue] = false;
				$parceFlag = $paramValue;
			}
			else
			{
				$args[$parceFlag] = $paramValue;
				$parceFlag = false;
			}
		}
		return $args;
	}
}
?>
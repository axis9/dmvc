<?
/*
	Универсальный класс для создания контейнера данных автоматическим созданием свойст.
	Получает значения в виде:

	$c = new DataContaier();
	$c->data = 1;
	$c->str = "aaaa";
	echo $c->data, PHP_EOL;
	echo $c->str;
*/
class DataContaier
{
	private $properties;
	public function __construct()
	{
		$this->properties = array();
	}

	public function __set($key, $value)
	{
		if (is_array($value))
		{
			$this->$key = $value;
		}
		else
		{
			$this->properties[$key] = $value;
		}
	}

	public function __get($key)
	{
		if (array_key_exists($key, $this->properties))
		{
			return $this->properties[$key];
		}
		return null;
	}
}
?>
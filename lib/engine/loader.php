<?
/*
	FАвтозагрузчик библиотек и моделей.
	Использует spl_autoload_register
	Требуется просто создать экземплар класса, дальше он сам будет подгружать билилиотеки из папки /lib
	Если не находит файл, то пробует подгрузить модель из папки /app/models
	Если не находит и там файлы или подгружает, но в памяти нужного класса нет, то вызывает исключение.
*/

class LibAutoLoader
{
	public function __construct()
	{
		spl_autoload_register('LibAutoLoader::Loader');
	}

	public static function Loader($ClassName)
	{

		$FullLibPath = LIB_PATH.$ClassName.'.php';
		$FullModelPath = MODELS_PATH.$ClassName.'.php';

		if(file_exists($FullLibPath))
		{
			$IncludePath = $FullLibPath;
		}
		elseif(file_exists($FullModelPath))
		{
			$IncludePath = $FullModelPath;
		}
		else
		{
			throw new Exception("Library/Model file {$ClassName} not found");
		}
		include($IncludePath);
		if(!class_exists($ClassName)) throw new Exception("Model/Lib class {$ClassName} not loaded from file {$IncludePath}");
	}
}
?>
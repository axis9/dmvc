<?
/*
	Парсин имен классов, пока не доделан.
	Предполагается, что в будущем обращение по именам или нахождение путей будет сделано через него
	Пока только при обращение через NameParcer::name_controller('aaaa') возвращает aaaaController
*/
class NameParcer
{
	//path_controller
	//path_view
	//
	public static function __callStatic($name, $arguments)
	{
		list($action, $target) = explode('_', $name);
		if($action=='path')
		{
			if($target=='controller')
			{
				//echo preg_replace('/^Controller$/', $replacement, $string);
			}
		}
		else if($action=='name')
		{
			if($target=='controller')
			{
				return preg_replace('/^(.*?)Controller$/', '$1', $arguments[0]);
			}
		}
	}
}
?>
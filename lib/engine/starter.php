<?
/*
	Класс для запуска контроллера
*/

class Starter
{
	private $Controller;
	private $Function;
	private $Status;
	//Подгружаем структуру вида:
	//stdClass Object ( [controller] => root [function] => index [params] => Array ( [] => ) )
	public function __construct($SiteStruct)
	{
		//если в полученной структуре указано, что подгружаем статический файл, то показываем выводим на экран файл с нужным content_type и останавливаем работу приложения.
		if(isset($SiteStruct->static_file))
		{
			$this->ShowFile($SiteStruct->static_file, $SiteStruct->content_type);
			exit();
		}

		//Если указан код, то выводим на экран страницу с кодом, например 404
		if(isset($SiteStruct->code))
		{
			$this->ShowCode($SiteStruct->code, $SiteStruct->code_memo);
			exit();
		}

		//определяем внутренние переменные и запускаем контроллер
		$this->Controller = $SiteStruct->controller.'Controller';
		$this->Function = $SiteStruct->function;
		$this->Params = $SiteStruct->params;
		$this->Move();
	}

	/*
		http://testmvc.loc/users/show/id/10
		Проверяем наличие контроллера usersController в системе, если OK, то создаем экземпляр класса контроллера, передаем в него параметры array(id=>10), полученные от пользователя
		и запускам метод show()

		Если контрлллер или метод не найден, то возвращаем страниу с кодом ошибки 404
	*/
	public function Move()
	{
		switch($this->checkController())
		{
			case OK:
				$controller = new $this->Controller();
				$controller->Args = $this->Params;
				$Func = $this->Function;
				$controller->$Func();
			break;
			default: NOT_FOUND:
				$this->ShowCode('404','Not Found');
			break;
		}
	}

	/*
		Показать пользователю ответ HTTP с кодом
		$Code - код HTTP
		$AddingInfo - подпись к коду
	*/
	public function ShowCode($Code, $AddingInfo)
	{
		/*
			Передаем через заголовок http, что возвращаем не код 200
			Возможно есть статичная страница с кодом 404, которая лежит в папке /public. Пишем ее полное имя, применяя константу из конфига
			Проверяем наличие, если нету, то просто выводим текст на экран, иначе грузим файл
		*/
		$FileWithCode = PUBLIC_PATH.$Code.'.html';
		header("HTTP/1.0 {$Code} {$AddingInfo}");
		if(file_exists($FileWithCode))
		{
			readfile($FileWithCode);
		}
		else
		{
			echo "<h1>{$Code} {$AddingInfo}</h1>";
		}
		exit();
	}

	/*
		$File - пусть к статичному файлу
		$ContentType - тип контента

		Проверяем наличие файла, если есть, то выводим на экран, иначе 404 ошибка
	*/
	public function ShowFile($File, $ContentType = "text/html")
	{
		if(file_exists($File))
		{
			header('Content-type: '.$ContentType);
			readfile($File);
		}
		else
		{
			$this->ShowCode('404','Not Found');
		}
	}

	/*
		Проверка наличия контроллера и метода
	*/
	private function checkController()
	{
		//Создаем полное имя до файла используя константу
		$ControllerName = $this->Controller;
		$ControllerFullPath = CONTROLLER_PATH.$ControllerName.'.php';
		//Если файл существует
		if(file_exists($ControllerFullPath))
		{
			//Подгружаем его
			//Если в памяти есть требуемый нам класс и метод, то возвращаем код OK, иначе код "NOT_FOUND" - не найден
			include($ControllerFullPath);
			if(class_exists($ControllerName) && method_exists($ControllerName,$this->Function))
			{
				return OK;
			}
		}
		return NOT_FOUND;
	}
}
?>
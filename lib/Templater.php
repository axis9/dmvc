<?
/*
	Элементраный шаблонизатор
	Простая и малофункциональная замена существующим шаблонизаторам
*/
class Templater
{
	//Значение layout - header и footer для вывода на экран, имя папки с этими файлами, обертка для вывода на экран View
	private $_layout;
	public function __construct()
	{
		//изначально layout указан по-умолчанию
		$this->_layout = DEFAULT_LAYOUT;
	}

	/*
		Смена layout
	*/
	public function ChooseLayout($layout = 'default')
	{
		$this->_layout = $layout;
	}

	/*
		Helper Pagination
		Для генерации списка ссылок для перехода между страницами
		$url_template - шаблон по которому надо выводить список страниц вида /users/[:page:] 
		[:page:] - номер страницы
		$total - общее количество записей
		$current - текущая страница
	*/
	public static function Pagination($url_template, $total, $current=false)
	{
		//получаем количество страниц с округлением в большую сторону
		//количество записей настранице берез из констант
		$total_pages = ceil($total / PER_PAGE);
		$html = "";
		/*
			если страниц больше чем 1
		*/
		if($total_pages>1)
		{
			/*
				циклом проходимся по каждой страница, заменяем значение [:page:] из шаблона и если указана текущая страница, то добавляем значение класса
				после возвращаем html строку
			*/
			for($i=0; $i<$total_pages; $i++)
			{
				$Page_url = str_replace('[:page:]', $i+1, $url_template);
				$html .= '<a href="'.$Page_url.'"'.($current == $i ? ' class="current"' : '').'>['.($i+1).']</a> ';
			}
		}
		return $html;
	}

	/*
		Вывести View на экран
		$view_name - имя контроллера
		Model - параметры, которые передаеются из контроллера в вид
	*/
	public function ShowView($view_name, $Model)
	{
		/*
			С помощью debug_backtrace() узнаем из какого метода вызывается Show, генерим путь до файта используя константу и $view_name
			Если файла нет, то вызываем исключение
		*/
		$Debug = debug_backtrace();

		$MethodCallName = $Debug[2]['function'];
		$ViewFullName = VIEWS_PATH.$view_name.'/'.$MethodCallName.'.php';

		if(!file_exists($ViewFullName))
		{
			throw new Exception($view_name.' -> '.$MethodCallName.' view not found');
		}

		/*
			$LayoutFound - layout найден
			$LayoutName - текущий layout
		*/
		$LayoutFound = false;
		$LayoutName = DEFAULT_LAYOUT;
		/*
			Если layout меняли, то меняем текущее значение
		*/
		if($this->_layout != DEFAULT_LAYOUT) 
		{
			$LayoutName = $this->_layout;
		}
		/*
			Генерим имена файлов и пути до файлов, проверяем их наличие
		*/
		$LayoutFullPath = LAYOUT_PATH.$LayoutName.'/';
		$LayoutHeader = $LayoutFullPath.'header.php';
		$LayoutFooter = $LayoutFullPath.'footer.php';
		if(file_exists($LayoutFullPath) && is_dir($LayoutFullPath) && file_exists($LayoutHeader) && file_exists($LayoutFooter))
		{
			/*
				layout найден
			*/
			$LayoutFound=true;
		}

		/*
			если layout найден, то выводим header, потом сам View, а дальше footer
		*/
		if($LayoutFound)
		{
			include($LayoutHeader);
		}

		include($ViewFullName);

		if($LayoutFound)
		{
			include($LayoutFooter);
		}
	}

	/*
		Включение на страницу частичных шаблонов, таких как меню, подключение css
		Вид Templater::AddPartialmenu
		где "menu" - имя файла _menu.php в папук /app/views/partials
	*/
	public static function __callStatic($name, $args)
	{
		//AddPartialmenu
		/*
			Парсим имя функции, генерим полное имя
			Если файл найден или название функции не верное, то выводим исключение
			Иначе подключаем файл
		*/
		$flag = preg_match('/^AddPartial(.*?)$/', $name, $out);
		if(!$flag) throw new Exception('Partial view not found');
		$partial_name = PARTIAL_PATH.'_'.$out[1].'.php';
		if(file_exists($partial_name))
		{
			include($partial_name);
		}
		else
		{
			throw new Exception('Partial '.$partial_name.' not found');
		}
	}

	/*
		Flash сообщение - хранится в сессии для разовой передачи между страницами
		выводится на страницу если в сесиии хранится flash сообщение
	*/
	public static function Flash()
	{
		if(isset($_SESSION[FLASH_MESSAGE_SESSION_PATH]) && strlen($_SESSION[FLASH_MESSAGE_SESSION_PATH])>0)
		{
			$FLASH_MESSAGE = $_SESSION[FLASH_MESSAGE_SESSION_PATH];
			include(PARTIAL_PATH.'flash_message.php');
		}
	}
}
?>
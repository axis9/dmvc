<?
/*
	Криптор паролей
	Обращение вида Criptor::encMYKEY('password')
	enc|dec - в начале указывает на действие - шифрование|дешифрование
	MYKEY - константа вида MYKEY_KEY, в которой указан ключ шифрования
*/
class Criptor
{
	public static function __callStatic($name, $args)
	{
		//разбираем название метода
		$flag = preg_match('/^(enc|dec)(.*?)$/', $name, $out);
		//если не подходит - вызываем исключение
		if(!$flag)
		{
			throw new Exception('Encription function not found');
		}
		//создаем имя константы
		$keyName = $out[2].'_KEY';

		//если константа не найдена, то вызываем исключение
		if(!defined($keyName))
		{
			throw new Exception('Chiper key not found');
		}

		$aes256Key = constant($keyName);

		/*создаем iv*/
		$iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND);

		/*шифрование*/
		if($out[1]=='enc')
		{
			return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $aes256Key, $args[0], MCRYPT_MODE_ECB, $iv));
		}
		/*дешифровка*/
		if($out[1]=='dec')
		{
			return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $aes256Key, base64_decode($args[0]), MCRYPT_MODE_ECB, $iv));
		}
	}
}
?>
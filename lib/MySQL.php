<?
/*
	MySQL класс для легкого обращения к базе через интерфейс PDO
	Паттерн singleton
*/
class MySQL
{
	private static $instance = null;
	public $Error;
	private $pdo;

	/*
		Enum константы для выборки конечного результата
		LAST_ID - вернуть id последнего auto_increment значения
		FETCH_ALL - вернуть массив значений выборки
		COUNT - вернуть количество измененых записей
	*/
	const LAST_ID=901;
	const FETCH_ALL=902;
	const COUNT=903;

	//подключение к базе
	public function __construct()
	{
		$this->pdo = new PDO('mysql:host='.MYSQL_HOST.';dbname='.MYSQL_DB, MYSQL_USERNAME, MYSQL_PASSWORD);
	}

	//выполение сложного запрос с конечной выборкой всех результатов.
	/*
		$sql - строка SQL запроса
	*/
	public function custom_query($sql)
	{
		return $this->ExecuteQuery($sql, false, self::FETCH_ALL);
	}

	/*
		select выборка
		$table - имя таблицы
		$entity - какие сущности выбрать
		$where - строка выборки вида "id = ? and username = ?"
		$where_params - массив значений для выборки where вида array(1, 'admin')
		$offset - с позиции какой записи сделать выборку
		$limit - сколько записей выбрать
		$order_by - по каким сущностям делать сортировку вида "id desc"
	*/
	public function select($table, $entity, $where, $where_params, $offset, $limit, $order_by=false)
	{
		//генерируем предварительный запрос
		$query = "SELECT ".implode(',',$entity)." FROM ".strtolower($table);
		//если where указан, то подставляем в запрос
		if($where!=false)
		{
			$query .= " WHERE ".$where;
		}

		//если сортировка указана, то добавляем в запрос
		if($order_by!=false)
		{
			$query.=' order by '.$order_by;
		}

		//добавляем в значение limit и offset
		if($offset!==false)
		{
			$query .= " limit ".$offset;
			if($limit!=false)
			{
				$query .= ", ".$limit;
			}
		}
		//выполняем запрос с передачей значения where и указанием выбора всех значений
		return $this->ExecuteQuery($query, $where_params, self::FETCH_ALL);
	}

	/*
		update запрос
		$table - таблица
		$entity - массив вида ключ-значение с указанием какие сущности надо обновлять
		$where - значение выборки для обновления вида "id = 10 or username = 'admin'"
	*/
	public function update($table, $entity, $where)
	{
		//обновляем значение даты
		$entity['update_date'] = NOW;
		/*
			Генерим строку SQL зпроса

			implode(', ',array_map(function($n) { return $n.' = ?'; }, array_keys($entity))

			array_map - функция, принимающая call-back фкнкцию для генерирования массива вида array(id => 10) в вид id = ?, передавая значения ключей массива $entity
			после array_map возвращает измененный массив и с помощью implode объединяет в строку для дальнейшей вставки в массив

		*/
		$query = "UPDATE ".strtolower($table)." {$table} set ".implode(', ',array_map(function($n) { return $n.' = ?'; }, array_keys($entity)));

		//если есть where, то добавляем в массив
		if($where!=false)
		{
			$query .= " WHERE ".$where;
		}
		//выполняем запрос с передачей значений массива $entity и указанием того, что надо вернуть только количество обновленных строк
		return $this->ExecuteQuery($query, array_values($entity), self::COUNT);
	}

	/*
		sql delete запрос
		$table - таблица
		$where - строка выборки вида "id = ? and username = ?"
		$where_params - массив значений для выборки where вида array(1, 'admin')
		$limit - сколько записей удалить
	*/
	public function delete($table, $where, $where_params, $limit=false)
	{
		//генерим запрос
		$query = "DELETE FROM ".strtolower($table);

		//подставляем where, если есть
		if($where!=false)
		{
			$query .= " WHERE ".$where;
		}
		//подставляем limit, если есть
		if($limit!=false)
		{
			$query .= " limit ".$limit;
		}
		//выполняем запрос с передачей массива $where_params с параметрами выборки и указанием того, что надо вернуть только количество удаленных строк
		return $this->ExecuteQuery($query, $where_params, self::COUNT);
	}

	/*
		insert sql запрос
		$Table - таблица
		$Args - массив вида ключ-значение вида array(id=>10, username=>'admin') для вставки в таблицу
	*/
	public function insert($Table, $Args)
	{
		$Table = strtolower($Table);

		//т.к. во всех таблицах есть сущности вида create_date и update_date то добавляем в них текущие даты
		$Args['create_date'] = NOW;
		$Args['update_date'] = NOW;

		//генерим значение аргументов в вид ":id, :username"
		$ArgsWithColon = array_map(function($n) { return ":".$n; }, array_keys($Args));

		//генерим SQL запрос
		$query = "INSERT INTO $Table (".implode(',',array_keys($Args)).") VALUES (".implode(',', $ArgsWithColon).")";
		//выполняем SQL запрос, вставляем значение, указание того, что надо вернуть последний вставленный id
		return $this->ExecuteQuery($query,$Args,self::LAST_ID);
	}

	/*
		Выполнение запроса
		$sql - строка SQL запроса
		$WhereArgs - параметры, которые вставляются после подготовки запроса страдствами PDO
		$RETURN_TYPE - какое знаение вернуть
	*/
	private function ExecuteQuery($sql, $WhereArgs=false, $RETURN_TYPE)
	{
		//подготавливаем запрос
		$query = $this->pdo->prepare($sql);
		//выполняем запрос с параметрами или без них
		$data = $WhereArgs !== false ? $query->execute($WhereArgs) : $query->execute();
		//внутреннее значение ошибки, требуется для отладки
		$this->Error = false;
		//если выполнился, то позвращаем значение, указанное в RETURN_TYPE
		if($data!==false)
		{
			switch ($RETURN_TYPE) {
				case self::LAST_ID: return $this->pdo->lastInsertId(); break;
				case self::FETCH_ALL: return $query->fetchAll(PDO::FETCH_ASSOC); break;
				case self::COUNT: return $query->rowCount(); break;
			}
		}
		//иначе записывам значение ошибки и возвращаем false
		$this->Error = $query->errorInfo();
		return false;
	}

	//singleton интерфейс
	public static function getInstance()
	{
		if (self::$instance == NULL)
		{
			self::$instance = new MySQL();
		}
		return self::$instance;
	}

	/*
		отключение клонирования
	*/
	private function __clone()
	{
	}
}
?>
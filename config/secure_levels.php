<?
	//уровень проверки логина юзеров
	//отключена проверка
	define('LEVEL_OFF', 330);
	//проверка включена
	define('LEVEL_LOGIN', 331);
	//время блокировки по часам
	define('LOCK_TIME_HOURS', 24);
	//unix формат
	define('LOCK_TIME_UNIX', LOCK_TIME_HOURS*60*60);
	//текущая дата + время блокировки
	define('LOCK_TIME_DATE', date('Y-m-d h:i:s',time() + LOCK_TIME_UNIX));
	//максимум попыток
	define('ATTEMPT_MAX', 5);
	//текущий уровень проверки
	define('CURRENT_SECURE_LEVEL', LEVEL_LOGIN);
?>
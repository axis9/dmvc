<?
	//путь к сайту
	define('SITE_PATH', getcwd());
	//конфиг
	define('CONFIG_PATH', SITE_PATH.'/config/');
	//папка приложения: контроллеры, виды
	define('APP_PATH', SITE_PATH.'/app');
	//библиотеки
	define('LIB_PATH', SITE_PATH.'/lib/');
	//библиотеки движка
	define('ENGINE_PATH', LIB_PATH.'engine/');
	//ктнтроллеры
	define('CONTROLLER_PATH', APP_PATH.'/controllers/');
	//прототипы
	define('PROTOTYPE_PATH', APP_PATH.'/prototypes/');
	//публичные файлы
	define('PUBLIC_PATH', SITE_PATH.'/public/');
	//виды
	define('VIEWS_PATH', APP_PATH.'/views/');
	//partial views
	define('PARTIAL_PATH', VIEWS_PATH.'partials/');
	//layouts
	define('LAYOUT_PATH', VIEWS_PATH.'/layout/');
	//css, js
	define('ASSETS_PATH', APP_PATH.'/assets');
	//модели
	define('MODELS_PATH', APP_PATH.'/models/');
	//laout по-умолчанию
	define('DEFAULT_LAYOUT', 'default');
	//код статичного файла
	define('CONTENT_STATIC', 1001);
	//код обращения к контроллеру
	define('CONTENT_CONTROLLER', 1000);
	//СЕССИИ
	//flash сообщение
	define('FLASH_MESSAGE_SESSION_PATH', 'flash_message');
	//id залогиненого юзера
	define('SECURE_SESSION_ID', 'secure_session_id');
	//ip залогиненого юзера
	define('SECURE_SESSION_IP', 'secure_session_ip_address');
?>
<?
//Основной класс запуска приложения
class Application
{
	public function __construct()
	{
		//при создании экземпляра класса запускаем bootstrap, загружающий основные библиотеки и конфиги
		$this->bootstrap();
	}

	public function bootstrap()
	{
		//Загружаем конфиги
		$this->loadConfig();
		//Загружаем библиотеки класса
		$this->loadEngineLib();
		//Загружаем прототипы для наследования
		$this->loadPrototypes();
		//Загружаем автозагрузчик библотек
		$AutoLoader = new LibAutoLoader();
		//Парсим ссылку
		$SiteStruct = Routing::UriParce();
		//Запускам стартер, загружающий контроллер
		$starter = new Starter($SiteStruct);
	}

	public function loadConfig()
	{
		$this->includeFilesFromFolder('config');
	}

	public function loadPrototypes()
	{
		//требуется сначала загрузить основные прототипы, а потом уже наследованные от них классы
		include(PROTOTYPE_PATH.'/_controller.php');
		include(PROTOTYPE_PATH.'/_model.php');
		$this->includeFilesFromFolder(PROTOTYPE_PATH, array('_controller.php', '_model.php'));
	}

	public function loadEngineLib()
	{
		$this->includeFilesFromFolder(ENGINE_PATH);
	}

	//подгрузчик файлов по папкам, есть возможность исключения файлов, чтобы избежать исключений при загрузке одного и того же класса
	/*
		$Path - путь к папке
		$exclude - массив с именами файлов, исключающие подключение
	*/
	private function includeFilesFromFolder($Path, $exclude = false)
	{
		if ($handle = opendir($Path))
		{
    		while(($entry=readdir($handle))!==false)
    		{
        		if($entry!="." && $entry!="..")
        		{
        			if($exclude!=false && in_array($entry, $exclude)) continue;
            		include($Path.'/'.$entry);
        		}
    		}
    		closedir($handle);
		}
	}
}
?>
<?
	//Кастомные роутинги, для изменения обращения
	/*
		Ключ - значение которое получаем от пользователя
		Все написано в регулярный выражениях, чтобы они работа корректно требуется экраниерование обратными слэшами
		Значение корня вида "/" надо указывать в "\/". Так же возможно что в ссылке в конце будет слэш или нет, тогда надо указывать значение выбора "ничего или слэш" (|\/)
		Так же можно получать параметры, например численные с указанием id или номера страницы. Они всегда будут иметь значение числа, то можно указать их видом ([0-9]+). Так же выходит неплохая защита от SQL инъекций

		end - конечное значение ссылки, в которое надо сконвертировать полученное, должно корректно соответсоввать значениею /контроллер/метод/параметры/через/слэш
		content
			- CONTENT_STATIC - статическая странице, не требуется обращение к контроллерам, надо писать путь до файла
			- CONTENT_CONTROLLER обращение к контроллеру
		method - роутинг работает только при обращение HTTP по этому методу, если метод не указан, то работают оба значения POST и GET
		content_type - какой тип контента, для корректного отображения и работы браузера. Если возвращать CSS и не указывать conent_type, то он не сработает. Неплохо было бы добавить возможность автоматического определения контента по расширению файла.

		Роутинга перебираются сверху вниз, если ничего не подошло, то срабатывает значение *
		* - универсальный указатель, указывающая, что полученная ссылка от пользователя корректно соответствует значению контроллера и метода. 
		Если убрать его, то обращаться к web приложению можно только через пути указаные ниже. Неплохая защита от нежелательного доступа и невнимательности программиста.

		Если ни один роутинг не подходит, а последний удален, то пользователю вернется страница с кодом 404

	*/
	$GlobalRoute = array(
		'\/' => array('end' => '/login/', 'content' => CONTENT_CONTROLLER),
		'\/login' => array('end' => '/login/login', 'content' => CONTENT_CONTROLLER, 'method' => 'POST'),
		'\/users(|\/)' => array('end' => '/users/index/Page/1', 'content' => CONTENT_CONTROLLER, 'method' => 'GET'),
		'\/users\/([0-9]+)' => array('end' => '/users/index/Page/$1', 'content' => CONTENT_CONTROLLER, 'method' => 'GET'),
		'\/user\/delete\/([0-9]+)' => array('end' => '/users/delete/id/$1', 'content' => CONTENT_CONTROLLER),
		'\/user\/create(|\/)' => array('end' => '/users/create/', 'content' => CONTENT_CONTROLLER, 'method' => 'GET'),
		'\/user\/create_user(|\/)' => array('end' => '/users/edit_new/', 'content' => CONTENT_CONTROLLER, 'method' => 'POST'),
		'\/user\/([0-9]+)(|\/)' => array('end' => '/users/show_user/id/$1', 'content' => CONTENT_CONTROLLER, 'method' => 'GET'),

		'\/user\/edit\/([0-9]+)(|\/)' => array('end' => '/users/edit/id/$1', 'content' => CONTENT_CONTROLLER, 'method' => 'GET'),
		'\/user\/edit_user(|\/)' => array('end' => '/users/edit_new/', 'content' => CONTENT_CONTROLLER, 'method' => 'POST'),
		'\/login_history(|\/)' => array('end' => '/users/login_history/Page/1', 'content' => CONTENT_CONTROLLER, 'method' => 'GET'),
		'\/login_history\/([0-9]+)(|\/)' => array('end' => '/users/login_history/Page/$1', 'content' => CONTENT_CONTROLLER, 'method' => 'GET'),
		// '\/css\/(.*?)' => array('end' => ASSETS_PATH.'/css/$1', 'content' => CONTENT_STATIC, 'content_type' => 'text/css'), //-- Added to .htaccess by mod_rewrite for optimisation
		// '\/js\/(.*?)' => array('end' => ASSETS_PATH.'/js/$1', 'content' => CONTENT_STATIC, 'content_type' => 'text/javascript'), //-- Added to .htaccess by mod_rewrite for optimisation
		'*' => array('*', CONTENT_CONTROLLER)
	);
?>
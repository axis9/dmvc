<div class="pagination"><?= Templater::Pagination('/users/[:page:]', $Model->UsersCount, $Model->CurrentPage) ?></div>
<table class='table'>
	<thead>
		<th>ID</th>
		<th>Username</th>
		<th>Password</th>
		<th>Current ip</th>
		<th>Lock date</th>
		<th>Login attempt</th>
		<th>Create date</th>
		<th>Update date</th>
		<th></th>
		<th></th>
	</thead>
	<tbody>
	<? if($Model->Users!==false) { ?>
	<? foreach ($Model->Users as $user) { ?>
		<tr id="tr<?= $user['id'] ?>">
			<td><?= $user['id'] ?></td>
			<td><?= $user['username'] ?></td>
			<td><?= $user['password'] ?></td>
			<td><?= $user['current_ip'] ?></td>
			<td><?= $user['lock_date'] ?></td>
			<td><?= $user['login_attempt'] ?></td>
			<td><?= $user['create_date'] ?></td>
			<td><?= $user['update_date'] ?></td>
			<td><a href='/user/edit/<?= $user['id'] ?>'>Edit</a></td>
			<td><a href='/user/delete/<?= $user['id'] ?>' class='deleteLink'>Delete</a></td>
		</tr>
	<? } ?>
	<? } ?>
	</tbody>
</table>
<div class="pagination"><?= Templater::Pagination('/login_history/[:page:]', $Model->login_history_count, $Model->CurrentPage) ?></div>
<table class='table'>
	<thead>
		<th>Username</th>
		<th>IP</th>
		<th>Create date</th>
	</thead>
	<tbody>
	<? if($Model->login_history!==false) { ?>
	<? foreach ($Model->login_history as $lh) { ?>
		<tr>
			<td><a href='/user/<?= $lh['id'] ?>'><?= $lh['username'] ?></a></td>
			<td><?= $lh['ip'] ?></td>
			<td><?= $lh['create_date'] ?></td>
		</tr>
	<? } ?>
	<? } ?>
	</tbody>
</table>
<?
/*
	Прототип контроллера
	Требуется для добавления методов по быстрому вызову шаблонизатора и передачи данных в View
*/
class ControllerPrototype
{
	//Экземпляр класса шаблонизатора
	private $Templater;
	//Переменная, через которую передаются данные в вид
	public $Model;
	//Переменные, которые передаются от пользователя в контроллер (GET)
	public $Args;

	public function __construct()
	{
		$this->Templater = new Templater();
		$this->Model = new DataContaier();
	}
	//Обертка, для передачи в шаблонизатор какой layout выбирать
	/*
		$layout - имя layout
	*/
	public function ChooseLayout($layout = 'default')
	{
		$this->Templater->ChooseLayout($layout);
	}
	//Запуск flash сообщения с редиректос
	/*
		$URL - куда редирект
		$text - какой текст передать в flash сообщении
	*/
	public function flash($URL, $text)
	{
		$_SESSION[FLASH_MESSAGE_SESSION_PATH] = $text;
		header('location: '.$URL);
		exit();
	}
	/*
		Показать вид текущего контрллера
		Обнулить flash сообщение, т.к. оно уже выведено на экран
	*/
	public function Show()
	{
		$this->Templater->ShowView(NameParcer::name_controller(get_called_class()), $this->Model);
		$_SESSION[FLASH_MESSAGE_SESSION_PATH] = '';
	}

	/*
		Вывести на экран JSON массив
		$json_array - массив данных
	*/
	public function ShowJSON($json_array)
	{
		header('Content-Type: application/json');
		echo json_encode($json_array);
	}
}
?>
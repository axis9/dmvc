<?
/*
	Прототип модели для наследования
	Связывает имя класса с таблицей в базе MySQL
	Требуется
		- добавить методы для быстрой вставки сразу нескольких записей
		- возможность для простого обращения к записям из других таблиц по foreign key
*/
class ModelPrototype extends ArrayObject
{
	//$properties массив для хранения значений сущностей модели полученных с помощью magic method _get _set
	private $properties = array();
	//экземпляр класса mysql
	public $db;
	//ошибка mysql
	public $Error;
	//массив бывших значений сущностей модели, требуется для сравнения какие значения зименены для оптимизации mysql update запроса
	private $OldProperties;
	//свойство модели - мождно обновлять в базе
	private $CanUpdate = false;
	//можно вставлять значение в базу
	private $CanInsert = true;

	public function __construct()
	{
		//получаем экземпляр класса mysql
		$this->db = MySQL::getInstance();
		//модель пустая, текущее значение id = 0
		$this->properties['id'] = 0;
	}

	/*
		сохраняем модели в базе
		в зависимости от свойств CanUpdate и CanInsert - обновляет значение или удаляет его
	*/
	public function save()
	{
		/*
			Запускаем hook beforesave validation - валиация значений перед сохранением
		*/
		$validation_hook = $this->Hooks('beforesave', 'validation');
		//если сработал хоть один, то сохранение не возможно, не прошла валидация 
		if($validation_hook !== true) return false;

		//если может обновлять но не вставлять записи в таблица
		if($this->CanUpdate && !$this->CanInsert)
		{
			/*
				Сравниваем массив текущих значений и старых, которые были до обновления модели
				Если изменения не произошли, то возвращаем false и записываем значение ошибки, что зименения не произвдены
			*/
			$diff = array_diff_assoc($this->properties,$this->OldProperties);
			if(count($diff)==0)
			{
				$this->Error = 'No change';
				return false;
			}
			/*
				Запускаем Hook beforesave filter - автоматическое изменение значений сущности перед сохраниением
				В данном примере - это шифрование пароля перед занесением его в базу
			*/
			$filter_hook = $this->Hooks('beforesave', 'filter');
			/*
				если хук не вернул "false", то значит отработал не верно, останавливаем сохранение
			*/
			if($filter_hook !== true) return false;
			/*
				обновляем значение в базе с выборкой по ID через обертку текущего прототипа модели
				возвращаем результат
			*/
			$result = $this->update($diff, "id = {$this->id}");
			$this->update_properties();
			return $result;
		}
		//если может вставлять значение, но не обновлять
		elseif(!$this->CanUpdate && $this->CanInsert)
		{
			/*
				Запускаем Hook beforesave filter
				Если хук не вернул "false", то значит отработал не верно, останавливаем сохранение
				Сохраняем значение, передавая имя дочернего класса как имя таблицы и массив properties, в котором хранятся значения модели
			*/
			$filter_hook = $this->Hooks('beforesave', 'filter');
			if($filter_hook !== true) return false;
			$result = $this->db->insert(get_called_class(), $this->properties);
		}
		//иначе ничего не делаем
		else
		{
			return false;
		}

		/*
			Если сохранилось, то  заменяем внутреннее значение ID на новое
			Меняем флаги. В любом случае модели после сохранения можно только обновлять
			Обновляем значения oldproperties
		*/
		if($result!==false)
		{
			$this->properties['id'] = $result;
			$this->CanUpdate = true;
			$this->CanInsert = false;
			$this->update_properties();
			return $this;
		}
		//Иначем записывем значение ошибки и возвращаем false
		$this->Error = $this->db->Error;

		return false;
	}

	/*
		delete - обертка для MySQL класса
		Два вида запуска:
			$Model->delete(); - удаление для существующей модели с записями
			$Model->delete('id > ?', '1', 10); - удаление для пустой модели с записями

		$where - условие выборки
		$where_params - массив значений для выборки
		$limit - сколько записей удалить, если false, то удалить все
	*/
	public function delete($where=false, $where_params=false, $limit=false)
	{
		//Если модель заполнена, то можно удалять без аргументов
		if($this->CanUpdate && !$this->CanInsert && !$where && !$where_params && !$limit)
		{
			//Удаление через mysql класс с условиями по id
			$result = $this->db->delete(get_called_class(), 'id = ?', array($this->id));
			/*
				Если удалено, то меняем флаг на "можно вставлять значения в базу, но не удалять"
				Стираем значение id в модели
			*/
			if($result!==false)
			{
				$this->CanUpdate = false;
				$this->CanInsert = true;
				unset($this->id);
			}
			//Иначе возвращаем отрицательный результат
			return $result;
		}
		else
		{
			/*
				Удаление через mysql класс по условиям из аргументов метода
			*/
			$result = $this->db->delete(get_called_class(), $where, $where_params, $limit);
			/*
				Если удалено, то возвращаем результат (количество удаленных строк)
			*/
			if($result!==false)
			{
				return $result;
			}
		}
		/*
			Иначе записываем ошибку и возвращаем false
		*/
		$this->Error = $this->db->Error;
		return $result;
	}

	/*
		select - обертка для MySQL класса
		$select_entity - какие значения выбирать строка/массив
		$where -  условие выборки
		$where_params - массив значений для выборки
		$offset - с какой записи делать выборку
		$limit - сколько записей удалить, если false, то удалить все
	*/
	public function select($select_entity, $where=false, $where_params=false, $offset=false, $limit=false)
	{
		/*
			Если ргументы select_entity и where_params не массивы, то конвертим в массив
		*/
		$convert = array('select_entity', 'where_params');
		foreach($convert as $convert_agr)
		{
			if(!is_array($$convert_agr))
			{
				$$convert_agr = array($$convert_agr);
			}
		}
		/*
			Делаем выборку, в значение таблицы подставляем имя класса
			Остальные параметры берем из аргументов текущей функции
		*/
		$all_rows = $this->db->select(get_called_class(), $select_entity, $where, $where_params, $offset, $limit);

		/*
			Если выборка не произошла, то записываем ошибку и возвращаем false
		*/
		if($all_rows==false)
		{
			$this->Error = $this->db->Error;
			return false;
		}
		/*
			Если пустой результат и возвращаем false
		*/
		if(count($all_rows)==0)
		{
			return false;
		}

		/*
			Запуск Hook afterselect filter
			Изменяет значения после получения их из базы
			В данном примере используется для расшифровки пароля
		*/

		//Массив найденных хуков
		$founded_methods = array();
		//перебираем названия сущностей полученных данных, для того, чтобы не вызывать хуки для не выбранных данных
		foreach(array_keys($all_rows[0]) as $entity)
		{
			//генерим название хука по имени "столбца" данных
			$filter_method = "afterselect_{$entity}_filter";
			//если метод найден, то записываем в массив
			if(method_exists($this,$filter_method))
			{
				$founded_methods[] = $entity;
			}
		}
		//перебираем все полученные поесле select записи
		foreach ($all_rows as &$row)
		{
			//перебираем каждый раз массив названий найденных хуков
			foreach ($founded_methods as $method)
			{
				//генерим имя метода
				$method_name = "afterselect_{$method}_filter";
				//запускаем его, передавая значение сущности, которое должно быть изменено, в данном случае это только зашифрованный пароль из базы
				$temp_result = $this->$method_name($row[$method]);
				//если изменение произошло, то заменяем значение
				if($temp_result!==false)
				{
					$row[$method] = $temp_result;
				}
			}
		}
		//Возвращаем массив данных
		return $all_rows;
	}

	/*
		update обертка для mysql класса
		$update_entity - ассоциативный массив данных для обновления вида array(username => 'aaaaa')
		$where - условие выборки обновления вида "id=10"
	*/
	public function update($update_entity, $where=false)
	{
		$result = $this->db->update(get_called_class(), $update_entity, $where);
		$this->Error = $this->db->Error;
		return $result;
	}

	/*
		Запускает хуки валидации, изменения данных до обновления
		Перебирает по именам, ищет методы в модели, запускает, если результат не false, то записывает ответ как ошибку и обрывает перебор
	*/
	public function Hooks($hook, $action)
	{
		foreach ($this->properties as $key => $value)
		{
			$validation_method = $hook.'_'.$key.'_'.$action;
			if(method_exists($this,$validation_method))
			{
				$validation = $this->$validation_method();
				if($validation !== false)
				{
					$this->Error = $validation;
					return false;
				}
			}
		}
		return true;
	}

	/*
		Helper для валидации поиска уникальных значений.
		Определяет из какого хука запущен с помощью debug_backtrace
		Генерит запрос и возвращает значение count
		Если значение больше 0 (значение не уникально), то возвращает true
		Иначе false
	*/
	public function CheckUnique()
	{
		$Debug = debug_backtrace();
		list(,$entity,) = explode('_',$Debug[1]['function']);
		$query = $this->select('count(*) as cnt', 'username = ? and id != ?', array($this->$entity, $this->id));
		return $query[0]['cnt']>0;
	}
	//Возвращает количество записей в таблице этой модели
	public function Count()
	{
		$query = $this->select('count(*) as cnt');
		return $query[0]['cnt'];
	}

	/*
		Обертка для mysql класса для быстрой выборки значений из базы
		Вид:
			findOnebyid(1)
			findLastOnebyid(1)
			findAllbyidandusername(1, 'admin')
	*/
	public function __call($name, $args)
	{
		/*
			Парсим имя метода, определяем:
				- Сколько записей вырвать - одну или все
				- Последнюю запись или первую
				- По каким значениям делать выборку
			Если не вычисляется, то возвращаем false
		*/
		$pregFlag = preg_match('/^find(Last|)(One|All)by(.*?)$/', $name, $out);
		if($pregFlag==false) return false;

		/*
			Часть с именами параметров разделяем на массив по "And"
		*/
		$entity = explode('And',$out[3]);
		//Выборка с первой записи
		$offset = false;
		//Выбираем только одно значение
		$limit = 1;
		//Если в имени функции указывается, что выбираем одно значение, то делаем выборку одной записи с нулевой позиции
		if($out[2] == 'One')
		{
			$offset = 0;
			$limit = 1;
		}
		/*
			Сортировка, для выборки первой или последне записи
			Если в имени функции указано "Last", то сортируем по id от большего к меньшему
		*/
		$order = '';
		if($out[1]=='Last')
		{
			$order = 'id desc';
		}
		/*
			Делаем выборку, генерим условия выборки через array_map
		*/
		$selected = $this->select(array('*'), implode(' and ',array_map(function($n) { return $n." = ?"; }, $entity)), array_slice($args, 0, count($entity)),$offset, $limit,$order);
		//Если выбираем одно значение
		if($out[2]=='One')
		{
			//Если выборка не произведена, то записывем ошибку и возвращаем false
			if($selected==false)
			{
				$this->Error = $this->db->Error;
				return false;
			}
			/*
				Значения текущей модели заменяем на полученные, превращая экземпляр класса в модель
				Обновляем старые значения на новые
				Меняем флаги
				Возвращаем экземпляр мождели как результат
			*/
			$this->properties = $selected[0];
			$this->update_properties();
			$this->CanUpdate = true;
			$this->CanInsert = false;
			return $this;
		}
		//Если все записи выбирались, то возвращаем массив
		return $selected;
	}
	/*
		Требуется для обновления Старых значений и Новых значений модели
		Когда моель сохраняется, то все ее сущности хранятся в $this->properties массиве и дублируются в $this->OldProperties
		Когда идет обновление модели, новые значения записываются только в $this->properties, после сравняиваются с $this->OldProperties перед сохранением и генерится запрос только на обновленные данные
	*/
	private function update_properties()
	{
		$this->OldProperties = $this->properties;
	}

	/*
		Требуется для ArrayObject интерфейса, чтобы обращаться к модели как массиву или объекту (__set __get)

	*/
	public function offsetGet($name)
	{
		return $this->properties[$name];
	}

	public function offsetSet($name, $value)
	{
		$this->properties[$name] = $value;
	}

	public function __set($key, $value)
	{
		if (is_array($value))
		{
			$this->$key = $value;
		}
		else
		{
			$this->properties[$key] = $value;
		}
	}

	public function __get($key)
	{
		if (array_key_exists($key, $this->properties))
		{
			return $this->properties[$key];
		}
		return null;
	}
}
?>
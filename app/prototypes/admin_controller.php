<?
/*
	Наследуется от основного прототипа
	При создании экземпляра класса меняется layout и проверяется сессия
	Если проверка не проходит, то обнуляем сессию и переходит на главную страницу
*/
class AdminControllerPrototype extends ControllerPrototype
{
	public function __construct()
	{
		parent::__construct();
		$this->ChooseLayout('admin');
		$check_session = new Login_ip();
		$check = $check_session->check_session();
		if($check!==true)
		{
			session_unset();
			$this->flash('/', $check);
		}
	}
}
?>
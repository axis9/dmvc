<?
/*
	Модель Login_history
	Для получения и сохранения модели в таблицу login_history
*/
class Login_history extends ModelPrototype
{
	public function __construct()
	{
		parent::__construct();
	}

	/*
		Метод для получения массива через сложного запроса к базе, практически как View в базах
		$Page - страница
	*/
	public function list_with_name($Page)
	{
		$db = MySQL::getInstance();
		$result = $db->custom_query('SELECT u.id, u.username, l.ip, l.create_date FROM login_history l inner join users u on u.id = l.user_id order by l.create_date desc limit '.(PER_PAGE*$Page).', '.PER_PAGE);
		return $result;
	}
}
?>
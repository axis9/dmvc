<?
/*
	Users модель, наследована от прототипа
	В mysql базе уже есть таблица users
	В ней по-умолчанию должны быть столбцы id, create_date, update_date
*/
class Users extends ModelPrototype
{
	/*
		Если есть id, то обновляем модель чере findOnebyid
		$id - id пользователя из таблицы Users
	*/
	public function __construct($id=false)
	{
		parent::__construct();
		$id = (int)$id;
		if($id!==false)
		{
			$a = $this->findOnebyid($id);
		}
	}

	/*
		Блокировка пользователя
		Выставление даты до какого числа учетка не активна и обнуляем количество попыток логина
	*/
	public function lock()
	{
		$this->lock_date = LOCK_TIME_DATE;
		$this->login_attempt = 0;
		$this->save();
	}
	/*
		Проверка учетной записи юзера
		Если дата блокировки не null или текущее время сервера в unix формате больше даты блокировки, то учетка активна
		Иначе пользователь заблокирован
	*/
	public function check_lock()
	{
		if($this->lock_date!=null)
		{
			return strtotime(NOW)>=strtotime($this->lock_date);
		}
		return true;
	}

	/*
		Хук валидации сущности username перед сохранением
		Чтобы валидация прошла - должен возвращаться false
	*/
	public function beforesave_username_validation()
	{
		/*
			Длина имени пользователя болше 3 и меньше 12 символов
		*/
		if(strlen($this->username)<3 || strlen($this->username)>12)
		{
			return 'Username length must be between 3 and 12 chars';
		}
		/*
			Имя содержит только буквы, цифры и знак подчеркивания
		*/
		if(!preg_match('/^[a-zA-Z0-9_]+$/', $this->username))
		{
			return 'Username must contain only chars and digits';
		}
		/*
			Имя уникальное
		*/
		if($this->CheckUnique())
		{
			return 'Username must be unique';
		}
		return false;
	}

	/*
		Хук изменения данных перед сохранением в базу
		В данном случае автоматически шифрует пароль перед сохранением в базу
		Если паоль меньше 8 или больше 32 символов, то выдает ошибку и не дает сохранить значение
	*/
	public function beforesave_password_filter()
	{
		if(strlen($this->password)<8 || strlen($this->password)>32)
		{
			return 'Password length must be between 8 and 32 chars';
		}
		// $this->current_ip = '0.0.0.1';
		$this->password = Criptor::encUSERPASSWORD($this->password);
		return false;
	}

	/*
		Хук изменяет значение сущности после выборки
		В данном случае расшифровывает пароль
	*/
	public function afterselect_password_filter($password)
	{
		return Criptor::decUSERPASSWORD($password);
	}
}
?>
<?
/*
	Модель хранения IP blacklist и проверки сессий
*/
class Login_ip extends ModelPrototype
{
	/*
		Ошибка логина
	*/
	public $LoginError='';

	public function __construct()
	{
		parent::__construct();
	}

	/*
		Проверяет сессию
		Если сессии нет или пользователь не найден или сохраненный IP не соответствует текущему, то возвращает ошибку
	*/
	public function check_session()
	{
		if(!isset($_SESSION[SECURE_SESSION_ID]))
		{
			return 'You not logged';
		}

		$session_user = new Users($_SESSION[SECURE_SESSION_ID]);
		if($session_user->id!==$_SESSION[SECURE_SESSION_ID])
		{
			return 'User not found';
		}

		if($_SESSION[SECURE_SESSION_IP]!=$_SERVER['REMOTE_ADDR'])
		{
			return 'Login from another IP';
		}

		return true;
	}

	/*
		Проверка пользователя при логировании
		$login - имя пользователя
		$password - пароль пользователя
	*/
	public function check_login($login, $password)
	{
		/*
			Проверка на нахождении IP в black листе
		*/
		if(!$this->check_ip_lock())
		{
			$this->LoginError = 'IP locked';
			return false;
		}

		/*
			Пытаемся найти пользователя по username
		*/
		$user = new Users();
		$check = $user->findOnebyusername($_POST['username']);

		//Если найден
		if($check!=false)
		{
			//Проверяем - требуется ли проверять сессию
			//Если да, то проверяем не заблокирован ли пользователь
			if(CURRENT_SECURE_LEVEL != LEVEL_OFF && !$user->check_lock())
			{
				$this->LoginError = 'User locked';
				return false;
			}
			//Если все нормально, то сверяем полученный пароль с паролем из базы
			if($user->password == $_POST['password'])
			{
				/*
					Если сходится, то
					Обнуляем количество попыток Входа
					Стираем дату блокировки
					Сохраняем текущий IP адрес
				*/
				$user->login_attempt = 0;
				$user->lock_date = null;
				$user->current_ip = $_SERVER['REMOTE_ADDR'];
				$user->save();

				/*
					Записываем историю
				*/
				$login_history = new Login_history();
				$login_history->user_id = $user->id;
				$login_history->ip = $_SERVER['REMOTE_ADDR'];
				$login_history->save();

				/*
					Сохраняем сессию
				*/
				$_SESSION[SECURE_SESSION_ID] = $user->id;
				$_SESSION[SECURE_SESSION_IP] = $_SERVER['REMOTE_ADDR'];

				return true;
			}
			/*
				Иначе записываем в базу, что не смогли залогинится и выводим ошибку
			*/
			$this->LoginError = 'User not found or password is incorrect';
			$this->note_bad_access($user->id);
			return false;
		}
		//Иначе пользователь не найден и записываем в базу
		else
		{
			$this->LoginError = 'User not found or password is incorrect';
			$this->note_bad_access();
		}
		return false;
	}

	//Проверка IP по black list
	public function check_ip_lock()
	{
		//Проверяем нужна ли проверка или нет
		if(CURRENT_SECURE_LEVEL == LEVEL_OFF)
		{
			return true;
		}
		/*
			Выбираем последнее значение из таблицы
		*/
		$Checkip = new Login_ip();
		$check = $Checkip->findLastOnebyip($_SERVER['REMOTE_ADDR']);

		/*
			Сравниваем тукущую дату с датой блокировки, если
				- Дата блокировки истекла, то можно удалять запись
				- Если дата не истекла и количество попыток логина больше максимального, то запрещяем вход
				- Иначе Вход разрешает
		*/
		if(strtotime(NOW) < (strtotime($check->create_date) + LOCK_TIME_UNIX) && $check->attempt>ATTEMPT_MAX)
		{
			return false;
		}
		elseif(strtotime(NOW) > (strtotime($check->create_date) + LOCK_TIME_UNIX))
		{
			$Checkip->delete();
			return true;
		}
		return true;
	}

	/*
		Запись неудачного логина, для дальнейшего разрешения
		$user_id - id пользователя
	*/
	public function note_bad_access($user_id=false)
	{
		//Проверяем нужна ли проверка или нет
		if(CURRENT_SECURE_LEVEL == LEVEL_OFF)
		{
			return true;
		}
		/*
			Если user_id не указан, то записываем IP в black list
			Сначала проверяем наличие его в базе, если нету, то создаем новый и разрешаем войти в следущий раз
			Если в базе был, то проверяем дату создания, если (дата создания + время блокировки) истекло, то обнуляем счетчик попыток входа
			Иначе прибавляем одну попытку и сохраняем
		*/
		if($user_id === false)
		{
			$l = $this->findLastOnebyip($_SERVER['REMOTE_ADDR']);
			if(!$l)
			{
				$this->ip = $_SERVER['REMOTE_ADDR'];
				$this->attempt = 1;
				$this->save();
				return true;
			}
			else
			{
				if(strtotime(NOW) >= (strtotime($this->create_date)+LOCK_TIME_UNIX))
				{
					$this->attempt = 1;
					$this->save();
					return true;
				}

				$this->attempt = $l->attempt + 1;
				$this->save();
				return true;
			}
		}
		/*
			Если user_id указан
			То получаем пользователя, увеличиваем количество попыток входа, если дата блокировки истекла, то обнуляем счетчик
			Иначе блокируем пользователя
		*/
		else
		{
			$user = new Users();
			$current_user = $user->findOnebyid($user_id);

			if($current_user->login_attempt>=ATTEMPT_MAX)
			{
				$current_user->lock();
				return true;
			}

			if(strtotime(NOW) >= (strtotime($this->lock_date)+LOCK_TIME_UNIX))
			{
				$current_user->login_attempt += 0;
			}
			$current_user->login_attempt += 1;
			$current_user->save();
			return true;
		}
	}
}
?>
$(document).ready(function(){
	$(".deleteLink").click(function(event){
		event.preventDefault();
		// console.log(this.href);
		var that = this;
		$.ajax(this.href, {
			type: 'post',
			timeout: 8000,
			dataType: 'json',
			success: function(json)
			{
				console.log(json);
				if(json.result==1)
				{
					tr = $(that).parent().parent();
					tr.css({'background-color' : 'red'}).fadeOut(function(){
						$(this).remove();
					});
				}
				else
				{
					console.log('no action');
				}
			},
			error: function(result)
			{
				console.log(result);
			}
		});
	});

	$("#saveForm").click(function(event){
		event.preventDefault();
		var form_data = $(this).parent().serialize();

		$.ajax($(this).parent().attr('action'), {
			type: 'post',
			timeout: 8000,
			dataType: 'json',
			data: form_data,
			beforeSend: function(){
				$("#saveForm").attr("disabled", "disabled");
				$("#username").attr("disabled", "disabled");
				$("#pass").attr("disabled", "disabled");
				$("#pass_again").attr("disabled", "disabled");
			},
			complete: function(){
				$("#saveForm").removeAttr("disabled");
				$("#username").removeAttr("disabled");
				$("#pass").removeAttr("disabled");
				$("#pass_again").removeAttr("disabled");
			},
			success: function(json)
			{
				var username = $("#username").val();
				if(json.status==1)
				{
					$(".notification").css({'background-color' : '#adddc3'}).show(function(){
						$(this).html(username + ' made. Redirect after 3 seconds...');
					});
					$("#username").val('');
					$("#pass").val('');
					$("#pass_again").val('');
					setTimeout(function(){
						window.location = json.redirect_link;
					}, 3000);
				}
				else
				{
					$(".notification").css({'background-color' : '#ddadaf'}).show(function(){
						$(this).html('Error! '+ json.errorMemo);
					});
					// console.log(json.errorMemo);
				}
			},
			error: function(result)
			{
				$(".notification").css({'background-color' : '#ddadaf'}).show(function(){
					$(this).html('Error! Connection problems!');
				});
			}
		});
	});
});
<?
/*
	Контроллер для работы с пользователями
*/
class usersController extends AdminControllerPrototype
{
	/*
		Модель для использования в базе
	*/
	private $Users;

	public function __construct()
	{
		parent::__construct();
		$this->Users = new Users();
	}
	/*
		GET /users/
		Показывает всех юзеров с разделением по страницам
		Передает в View общее количесво записей в таблице и текущую страницу, чтобы partial смог показать список страниц
	*/
	public function index()
	{
		$Page = abs((int)$this->Args['Page']-1);
		$result = $this->Users->select('*', false, false, PER_PAGE*$Page,PER_PAGE);
		$this->Model->Users = $result;
		$this->Model->UsersCount = $this->Users->Count();
		$this->Model->CurrentPage = $Page;
		$this->Show();
	}
	/*
		GET /user/:id
		Показывает отдельного юзера по id
	*/
	public function show_user()
	{
		$u = new Users();
		$this->Model->User = $u->findOnebyid((int)$this->Args['id']);
		$this->Show();
	}
	/*
		GET /user/create
		Выводит форму создания пользователя, которая сохраняет пользователей через ajax
	*/
	public function create()
	{
		$this->Show();
	}

	/*
		POST /user/create_user
		Создание или редактирование пользователя, работает через ajax
	*/
	public function edit_new()
	{
		/*
			Получение id пользователя (передается только при редактировании)
		*/
		$id = isset($_POST['id']) ? (int)$_POST['id'] : false;
		$json = array();
		/*
			Проверка пароля
		*/
		if($_POST['password'] == $_POST['password_again'])
		{
			/*
				Если id передан, то пытаемся его подгрузить из базы
				Если не получается, то редиректим на страницу пользователей с шибкой
				Иначе именяем значения сущностей и сохраняем
			*/
			if($id!==false)
			{
				$u = new Users();
				$new_user = $u->findOnebyid($id);
				if($new_user==false) $this->flash('/users/','User not found');
				$new_user->username = trim($_POST['username']);
				$new_user->password = trim($_POST['password']);
				$result = $new_user->save();
				if($result!=false)
				{
					$result = $new_user;
				}
			}
			/*
				Если id не было, то создаем пользователя
			*/
			else
			{
				$u = new Users();
				$u->username = trim($_POST['username']);
				$u->password = trim($_POST['password']);
				$result = $u->save();
			}
			/*
				Если сохранен, то выводит json со статусом status => '1', id пользователя и прямую ссылку до него
				Иначе status => 0 и ошибка
			*/
			if($result === false)
			{
				// $this->flash('/user/new/',$u->Error);
				$json = array('status' => '0', 'errorMemo' => $u->Error);
			}
			else
			{
				$json = array('status' => '1', 'id' => $result->id, 'redirect_link' => '/user/'.$result->id, 'errorMemo' => '',);
			}
		}
		else
		{
			/*
				Пароли не сходятся
			*/
			$json = array('status' => '0', 'errorMemo' => 'Passwords not same');
		}
		$this->ShowJSON($json);
	}
	/*
		GET /user/edit/:id
		Показать форму редактирования пользователя
		Сохранение через ajax
	*/
	public function edit()
	{
		$u = new Users();
		$user = $u->findOnebyid((int)$this->Args['id']);
		if($user==false)
		{
			$this->flash('/users/','User not found');
		}
		$this->Model->User = $user;
		$this->Show();
	}

	/*
		GET /user/login_history/
		Получение и вывод на экран истории логинов
	*/
	public function login_history()
	{
		$Page = abs((int)$this->Args['Page']-1);
		$login_history = new Login_history();
		$this->Model->login_history = $login_history->list_with_name($Page);
		$this->Model->login_history_count = $login_history->Count();
		$this->Model->CurrentPage = $Page;
		$this->Show();
	}

	/*
		POST /user/delete
		Удаление пользователя
		Получаем id от POST
		Находим юзера, если инаходим, то возвращаем result => 1, иначе result => 0
	*/
	public function delete()
	{
		$id = abs((int)$this->Args['id']);
		$u = new Users();
		$user = $u->findOnebyid($id);
		$result = 0;
		if($user!=false)
		{
			$result = $user->delete();
		}
		$json = array('result' => 0);
		if($result != 0)
		{
			$json['result'] = 1;
		}
		$this->ShowJSON($json);
	}

	/*
		GET /users/base_generation
		Автоматическая генерация пользователей с паролями
		Сделано для тестов
	*/
	public function base_generation()
	{
		$usernames = array(
		'AkHolic' => 'vie69myopE$',
		'FunnyMonkey' => 'luv43soja34%',
		'spamalot' => 'life#tWAE=10',
		'imurdaddy' => 'hOoD19IAmbi~',
		'kissme' => 'pie+laird93',
		'ustink' => 'bRan+fizzy98',
		'chuckynoris' => 'waP87Pein93~',
		'nutjob' => 'New:Wap*MOn84',
		'Cookieder' => 'soD70moAs67@',
		'EatmySts' => 'Zoa46paRRS~',
		'LookatMe' => 'orb$BAgs&Bug52',
		'AssKicker' => 'BOHeA28feck*',
		'Lovely01' => 'WHiLe&baSsi37',
		'Cooldu1990' => 'kAf48hoy25#',
		'TrendC101' => 'Scald~Thud76',
		'back2back' => 'lInt44Hut=',
		'3raser' => 'amoRt=YoB45');

		foreach ($usernames as $user => $password) {
			$NewUser = new Users();
			$NewUser->username = $user;
			$NewUser->password = $password;
			$result = $NewUser->save();
			if($result==false)
			{
				$this->flash('/users/', $user.': '.$NewUser->Error);
			}
		}
		header('location: /users');
	}
}
?>
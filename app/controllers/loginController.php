<?
//login controller
class loginController extends ControllerPrototype
{
	public function __construct()
	{
		parent::__construct();
	}

	/*
		/login/login
		Попытка залогинится, если username или password пустые или логин не проходит, то редиректит в корень сайта
		Иначе показывает страницу пользователей
	*/
	public function login()
	{
		$login = new Login_ip();
		if(empty($_POST['username']) || empty($_POST['password']))
		{
			$this->flash('/', 'Username or password fiel empty');
		}

		if(!$login->check_login($_POST['username'], $_POST['password']))
		{
			$this->flash('/', $login->LoginError);
		}
		else
		{
			header('location: /users');
		}
	}

	/*
		/login/add_admin_user

		Добавляет пользователя admin:password в базу
		Сделано для теста
	*/
	public function add_adimn_user()
	{
		$user = new Users();
		$user->username='admin';
		$user->password='password';
		$user->save();
		header('login: /');
	}

	/*
		/
		Показывает страницу логина
	*/
	public function index()
	{
		$this->Show();
	}
}
?>